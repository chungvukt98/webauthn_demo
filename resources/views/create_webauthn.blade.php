@extends('larapass::layout')
@section('title')
create webauthn
@endsection
@section('page_name')
create_webauth
@endsection
@section('body')
<div class="login-box">
    <!-- /.login-logo -->
    <div class="text-center mb-3">
        <strong class="title_login"><img src="{{ asset('images/logo.png') }}" alt="" style="max-width: 100%; height:50px"></strong>
    </div>
    <div class="card card-outline card-primary">
        <div class="card-body">
            <form id="register-form">
                @csrf
                <div class="input-group mb-3">
                    <input type="text" name="username" class="form-control" {{ old('login_id') }} placeholder="username or email">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <!-- /.col -->
                    <div>
                        <button type="submit" class="btn btn-primary btn-block">register</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
@endsection
@push('scripts')
    <script src="{{ asset('vendor/larapass/js/larapass.js') }}"></script>

    <!-- Registering credentials -->
    <script>
        const register = (event) => {
            event.preventDefault()
            new Larapass({
                register: 'webauthn/register',
                registerOptions: 'webauthn/register/options'
            }).register()
            .then(response => alert('Registration successful!'))
            .catch(response =>console.log(response))
        }

        document.getElementById('register-form').addEventListener('submit', register)
    </script>
@endpush