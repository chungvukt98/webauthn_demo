<?php

use App\Http\Controllers\Auth\WebAuthnRegisterController;
use App\Http\Controllers\Auth\WebAuthnLoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\WebAuthnDeviceLostController;
use App\Http\Controllers\Auth\WebAuthnRecoveryController;
use App\Http\Controllers\AuthController;

Route::get('login', [AuthController::class, 'getLogin'])->name('get.login');
Route::post('login',[AuthController::class, 'postLogin'])->name('post.login');


     
Route::get('webauthn/lost', [WebAuthnDeviceLostController::class, 'showDeviceLostForm'])
->name('webauthn.lost.form');
Route::post('webauthn/lost', [WebAuthnDeviceLostController::class, 'sendRecoveryEmail'])
->name('webauthn.lost.send');

Route::get('webauthn/recover', [WebAuthnRecoveryController::class, 'showResetForm'])
->name('webauthn.recover.form');
Route::post('webauthn/recover/options', [WebAuthnRecoveryController::class, 'options'])
->name('webauthn.recover.options');
Route::post('webauthn/recover/register', [WebAuthnRecoveryController::class, 'recover'])
->name('webauthn.recover');

Route::middleware(['auth:web'])->group(function () {
     Route::get('/wellll', function () {
          return view('welcome');
     })->name('main');

     Route::get('create_webauthn',[AuthController::class, 'createWebauthn'])->name('create_webauthn');

Route::post('webauthn/register/options', [WebAuthnRegisterController::class, 'options'])
     ->name('webauthn.register.options');
Route::post('webauthn/register', [WebAuthnRegisterController::class, 'register'])
     ->name('webauthn.register');

Route::post('webauthn/login/options', [WebAuthnLoginController::class, 'options'])
     ->name('webauthn.login.options');
Route::post('webauthn/login', [WebAuthnLoginController::class, 'login'])
     ->name('webauthn.login');
});