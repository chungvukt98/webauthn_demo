<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

class AuthController extends Controller
{
    public function getLogin() {
        return view('login');
    }

    public function postLogin(Request $request) {
        $data = $request->only(['email', 'password']);
        if (Auth::guard('web')->attempt($data)) {
            return redirect()->intended(route('create_webauthn'));
        }
        $messageBag = new MessageBag([__('messages.auth.login_fail')]);

        return redirect()->back()->withInput()->withErrors($messageBag);
    }

    public function createWebauthn(Request $request) {
        return view('create_webauthn');
    }
}
